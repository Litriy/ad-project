import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import vuetify from './plugins/vuetify'
import * as fb from 'firebase'

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App),
  created () {
    fb.initializeApp({
      apiKey: 'AIzaSyC5zhX0j2L89xZwz8v-8wl9aGizCBVKrQ4',
      authDomain: 'itc-ads-ff37f.firebaseapp.com',
      databaseURL: 'https://itc-ads-ff37f.firebaseio.com',
      projectId: 'itc-ads-ff37f',
      storageBucket: 'itc-ads-ff37f.appspot.com',
      messagingSenderId: '351699744964',
      appId: '1:351699744964:web:a01ee2c46593dd45f13191'
    })
    fb.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })
    this.$store.dispatch('fetchAds')
  }
}).$mount('#app')
