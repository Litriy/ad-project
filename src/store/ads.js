
import * as fb from 'firebase'

class Ad {
  constructor (title = '', description = '', ownerId = null, imageSrc = '', promo = false, id = null) {
    this.title = title
    this.description = description
    this.ownerId = ownerId
    this.imageSrc = imageSrc
    this.promo = promo
    this.id = id
  }
}
export default {
  state: {
    ads: []
  },
  mutations: {
    createAd (state, payload) {
      state.ads.push(payload)
    },
    updateAd (state, { title, description, id }) {
      const ad = state.ads.find(a => {
        return a.id === id
      })
      ad.title = title
      ad.description = description
    },
    loadAds (state, payload) {
      state.ads = payload
    }
  },
  actions: {
    // Create Ad action start
    async createAd ({ commit, getters }, payload) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const newAd = new Ad(
          payload.title,
          payload.description,
          getters.user.id,
          null,
          payload.promo
        )
        let imageSrc
        let key
        fb.database().ref('ads').push(newAd)
          .then((data) => {
            key = data.key
            return key
          })
          .then(key => {
            const image = payload.image
            const ext = image.name.slice(image.name.lastIndexOf('.'))
            return fb.storage().ref(`ads/${key}.${ext}`).put(image)
          })
          .then(snapshot => {
            return new Promise((resolve, reject) => {
              snapshot.ref.getDownloadURL().then(url => {
                snapshot.downloadURL = url
                resolve(snapshot)
              })
            })
          })
          .then((snapshot) => {
            imageSrc = snapshot.downloadURL
            return fb.database().ref('ads').child(key).update({ imageSrc: imageSrc })
          })
          .then(() => {
            commit('setLoading', false)
            commit('createAd', {
              ...newAd,
              imageSrc: imageSrc,
              id: key
            })
          })
          .catch((error) => {
            console.log(error)
          })
      } catch (error) {
        commit('setError', error.message)
        commit('setLoading', false)
        console.log(error)
        throw error
      }
    },
    // Update Ad action start
    async updateAd ({ commit }, { title, description, id }) {
      commit('clearError')
      commit('setLoading', true)
      try {
        await fb.database().ref('ads').child(id).update({
          title, description
        })
        commit('updateAd', {
          title, description, id
        })
        commit('setLoading', false)
      } catch (error) {
        commit('setError', error.message)
        throw error
      }
    },
    // fetch Ads action start
    async fetchAds ({ commit }) {
      commit('clearError')
      commit('setLoading', true)
      const resultAds = []
      try {
        const fbVal = await fb.database.apply().ref('ads').once('value')
        const ads = fbVal.val()
        Object.keys(ads).forEach(key => {
          const ad = ads[key]
          resultAds.push(
            new Ad(ad.title, ad.description, ad.ownerId, ad.imageSrc, ad.promo, key)
          )
        })
        commit('loadAds', resultAds)
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', error.message)
        commit('setLoading', false)
      }
    }
  },
  getters: {
    ads (state) {
      return state.ads
    },
    promoAds (state) {
      return state.ads.filter(ad => {
        return ad.promo
      })
    },
    myAds (state) {
      return state.ads
    },
    adById (state) {
      return adId => {
        return state.ads.find(ad => ad.id === adId)
      }
    }
  }
}
